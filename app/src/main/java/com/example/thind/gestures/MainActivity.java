package com.example.thind.gestures;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    //View v;
    TextView tv;
    RelativeLayout relativeLayout;
    GradientDrawable GD;
    View circle;
    Button click;
    float a,b,x,y,centerX,centerY,radius,distance;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tv=(TextView)findViewById(R.id.tv);
        relativeLayout=(RelativeLayout)findViewById(R.id.relativelayout);
      //  GD=(GradientDrawable)findViewById(R.id.circle);
        circle=(View)findViewById(R.id.circle);
        click=(Button)findViewById(R.id.Enter);
        GD = (GradientDrawable)circle.getBackground();

        relativeLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
//                x = event.getX();
//                y = event.getY();
                a = circle.getLeft();
                b = circle.getRight();
            centerX=(circle.getLeft()+circle.getRight())/2;
            centerY=(circle.getTop()+circle.getBottom())/2;
             radius=(b-a)/2;
                distance=(float) Math.sqrt(Math.pow(centerX-event.getX(),2)+(Math.pow(centerY-event.getY(),2)));
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        if (distance<radius) {
                            tv.setText("Inside the circle");
                        }
                        else if (distance == radius)
                        {
                            tv.setText("On The Boundary");
                        }
                        else {
                            tv.setText("Outside the circle");
//                            GD.setColor(Color.GREEN);
                        }
                        break;
//                    case MotionEvent.ACTION_UP:
//                        tv.setText("touched");
//                        break;
                    case MotionEvent.ACTION_MOVE:
                        if (distance<radius) {
                            tv.setText("Inside the circle");
                            GD.setColor(Color.GREEN);
//                    return true;
                        }
                        else if (distance == radius)
                        {
                            tv.setText("On The Boundary");
                        }
                        else {
                            tv.setText("Outside the circle");
                            GD.setColor(Color.RED);
                        }
                        break;

                }
                return true;
                }
            });

       click.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               Intent next = new Intent(MainActivity.this, Activity2.class);
               startActivity(next);

           }
       });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
