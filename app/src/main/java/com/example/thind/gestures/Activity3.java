package com.example.thind.gestures;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TextView;

public class Activity3 extends AppCompatActivity implements View.OnTouchListener, View.OnClickListener {

    GridView gridView;
    GradientDrawable bgShape;
    Adapter gridAdapter;
    View viewCircle;
    float radius;
    float xCenter;
    float yCenter;
    TextView title,wrongcols,wrongrows;
    Button enterButton;
    EditText rows, cols;
    int numRows, numCols;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_3);
        init();
        setOnClick();
    }

    public void init() {
        gridView = (GridView) findViewById(R.id.grid);
        title = (TextView) findViewById(R.id.tv2);
        wrongrows = (TextView) findViewById(R.id.wrongrows);
        wrongcols = (TextView) findViewById(R.id.wrongcols);
        rows = (EditText) findViewById(R.id.rows);
        cols = (EditText) findViewById(R.id.cols);
        enterButton = (Button) findViewById(R.id.Enter);

    }

    public void setOnClick() {
        enterButton.setOnClickListener(Activity3.this);
    }

    public void hideKeybord() {

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm.isActive())
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        float currentXPosition = event.getX();
        float currentYPosition = event.getY();
        int position = gridView.pointToPosition((int) currentXPosition, (int) currentYPosition);
        if (position < 0) {
            title.setText("Outside The Circle");
            return false;
        }
        circleInfo(position);
        switch (event.getAction()) {
            case MotionEvent.ACTION_UP:
                touch(event.getX(), event.getY(),position);
                break;
            case MotionEvent.ACTION_DOWN:
                touch(event.getX(), event.getY(),position);

                break;
            case MotionEvent.ACTION_MOVE:
                touch(event.getX(), event.getY(), position);

                break;
        }

        return true;
    }

    public void touch(float axisX, float axisY,int position) {
        if (distance(axisX, axisY) < radius) {
            changeColor(1,position);
        } else if (distance(axisX, axisY) == radius) {
            changeColor(2,position);
        } else {
            changeColor(3,position);
        }
    }

    public void circleInfo(int pos) {
        viewCircle = gridView.getChildAt(pos);
        bgShape = (GradientDrawable) viewCircle.getBackground();
        radius = (viewCircle.getWidth()) / 2;

        xCenter = (viewCircle.getRight() + viewCircle.getLeft()) / 2;
        yCenter = (viewCircle.getBottom() + viewCircle.getTop()) / 2;
    }

    public float distance(float x, float y) {
        float distance;
        distance = (float) Math.sqrt(Math.pow((x - xCenter), 2) + Math.pow((y - yCenter), 2));
        return distance;
    }

    public void changeColor(int pos,int position) {
        switch (pos) {
            case 1:
                position++;
                bgShape.setColor(Color.GREEN);
                title.setText("Inside the circle "+position);
                break;
            case 3:
                bgShape.setColor(Color.RED);
                title.setText("Outside the circle");
                break;
        }
    }

    @Override
    public void onClick(View v) {
        hideKeybord();
        switch (v.getId()) {
            case R.id.Enter:
                numRows = Integer.parseInt(rows.getText().toString());
                numCols = Integer.parseInt(cols.getText().toString());
                if ((numRows > 0 && numRows < 5) && (numCols > 0 && numCols < 5)) {
                    wrongrows.setText("");
                    wrongcols.setText("");
                    title.setTextColor(Color.BLACK);
                    title.setText("");
                    float gridViewHeight = gridView.getHeight(), gridViewWidth = gridView.getWidth();
                    gridView.setNumColumns(numCols);
                    gridAdapter = new Adapter(Activity3.this, numRows, numCols, gridViewHeight, gridViewWidth);
                    gridView.setAdapter(gridAdapter);
                    gridAdapter.notifyDataSetChanged();
                    gridView.setOnTouchListener(Activity3.this);
                } else {
                    if (numRows >= 5 && numCols >= 5) {
                        wrongrows.setText("Enter Correct rows");
                        wrongcols.setText("Enter Correct columns");
                    } else if (numRows >= 5) {
                        wrongcols.setText("");
                        wrongrows.setText("Enter Correct rows");
                    } else if (numCols >= 5) {
                        wrongcols.setText("Enter Correct columns");
                        wrongrows.setText("");
                    }
                }
        }
    }
}
