package com.example.thind.gestures;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;

/**
 * Created by Thind on 12-10-2015.
 */
public class Adapter extends BaseAdapter {
    int totalRows, totalCols, widthoflayout, heightoflayout;
    Context context;

    public Adapter(Context context, int totalRows, int totalCols, float heightoflayout, float widthoflayout) {
        this.context = context;
        this.totalRows = totalRows;
        this.totalCols = totalCols;
        this.heightoflayout = (int) (heightoflayout / totalRows);
        this.widthoflayout = (int) (widthoflayout / totalCols);
    }

    @Override
    public int getCount() {
        return totalRows * totalCols;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (heightoflayout < widthoflayout)
            widthoflayout = heightoflayout;
        else if (widthoflayout < heightoflayout)
            heightoflayout = widthoflayout;

        AbsListView.LayoutParams lp = new AbsListView.LayoutParams(widthoflayout, heightoflayout);
        LayoutInflater li = LayoutInflater.from(context);
        convertView = li.inflate(R.layout.grid_column, parent, false);
        convertView.setLayoutParams(lp);
        return convertView;
    }
}
